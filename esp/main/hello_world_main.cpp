/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "NeoEsp32RmtMethod.h"
extern "C" {
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "m3/m3.h"
//#include "m3/m3_api_wasi.h"
#include "m3/m3_env.h"
#include "m3_util.h"
#include "../../rustwasm/target/wasm32-wasi/release/rustwasm.h"
}
#include "m3_espwasi.c"
#define FATAL(msg, ...) { printf("Fatal: " msg "\n", ##__VA_ARGS__); return; }


void wasm_task(void* arg) {
    M3Result result = c_m3Err_none;

    /*uint8_t* wasm = (uint8_t*)fib32_wasm;*/
    /*uint32_t fsize = fib32_wasm_len-1;*/

    uint8_t* wasm = (uint8_t*)target_wasm32_wasi_release_rustwasm_wasm;
    uint32_t fsize = target_wasm32_wasi_release_rustwasm_wasm_len-1;

    printf("Loading WebAssembly...\n");

    printf("New Environment...\n");
    IM3Environment env = m3_NewEnvironment ();
    if (!env) FATAL("m3_NewEnvironment failed");

    printf("New Runtime...\n");
    IM3Runtime runtime = m3_NewRuntime (env, 16*1024, NULL);
    //printf("runtime wasmpages: %p\n", runtime->memory.wasmPages);
    if (!runtime) FATAL("m3_NewRuntime");
    m3_PrintRuntimeInfo(runtime);

    printf("Parse Module...\n");
    IM3Module module;
    result = m3_ParseModule (env, & module, wasm, fsize);
    if (result) FATAL("m3_ParseModule: %s", result);

    printf("Load Module...\n");
    result = m3_LoadModule (runtime, module);
    if (result) FATAL("m3_LoadModule: %s", result);

    printf("Linking ESPWASI...\n");
    result = m3_LinkESPWASI(module);
    if(result) FATAL("m3_LinkESPWASI: %s", result);

    printf("Finding leds function...\n");

    IM3Function f;
    result = m3_FindFunction (&f, runtime, "leds");
    if (result) FATAL("m3_FindFunction: %s", result);

    printf("Running...\n");

    /*const char* i_argv[2] = { 24, NULL };*/
    /*result = m3_CallWithArgs (f, 1, i_argv);*/

    NeoSk6812Method leds(14, 50, 4);
    leds.Initialize();

    for(int i =0;i<2;i++) {
      printf("Calling...\n");
      result = m3_Call(f);

      m3stack_t stack = (m3stack_t)(runtime->stack);
      uint32_t returnInt = *((uint32_t*)stack);
      printf("Call Done...%i\n", returnInt);
      //printf("returned int: %d\n", returnInt);

      //memcpy(leds.getPixels(), &runtime->memory.wasmPages[returnInt], leds.getPixelsSize());
      memcpy(leds.getPixels(), offset2addr(runtime, returnInt), leds.getPixelsSize());

      leds.Update(true);
      //vTaskDelay(100/ portTICK_PERIOD_MS);
    }
    printf("Done...\n");
    

    //rgbwVal l1[2] = {makeRGBWVal(128,128,128,128), makeRGBWVal(128,128,128,0)};
    //sk6812_init(14);
    //sk6812_setColors(10, l1);
    
    vTaskDelete(NULL);


}

extern "C" void app_main()
{
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");


    xTaskCreate(&wasm_task, "wasm_m3", 32768, NULL, 5, NULL);

    vTaskDelete(NULL);
}
