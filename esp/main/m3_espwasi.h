#pragma once

#if defined(__cplusplus)
extern "C" {
#endif
#include "m3/m3_core.h"

M3Result m3_LinkESPWASI (IM3Module io_module);

#if defined(__cplusplus)
}
#endif

