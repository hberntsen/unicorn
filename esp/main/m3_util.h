#pragma once

#include "m3/m3.h"

static inline
uint32_t addr2offset(IM3Runtime m, void *addr) {
    return (u8*)addr - (u8*)(m->memory.mallocated+1);
}

static inline
void *offset2addr(IM3Runtime m, uint32_t offset) {
    return (u8*)(m->memory.mallocated+1) + offset;
}

