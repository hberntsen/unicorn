#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

WEE_ALLOC_STATIC_ARRAY_BACKEND_BYTES='262144' RUSTFLAGS='-C link-arg=--strip-all -C link-arg=-z -C link-arg=stack-size=16384' xargo build --target wasm32-wasi  --release --lib
xxd -i target/wasm32-wasi/release/rustwasm.wasm > target/wasm32-wasi/release/rustwasm.h
#wasm2wat target/wasm32-wasi/release/rustwasm.wasm  
